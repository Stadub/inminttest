﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {

            var app = new Program( 5);

            app.StartProducerAsync();
            app.StartConsumersAsync();

                Console.WriteLine("Press any key to restart Consumers");
                Console.ReadLine();
                app.StopConsumers();

                app.StartConsumersAsync();
                Console.WriteLine("Press any key to Stop Producer and Consumers");
                Console.ReadLine();
                app.StopProducer();
                app.StopConsumers();


            Console.WriteLine("Press again to exit");
            Console.ReadLine();
        }


        private BufferBlock<int> buffer;
        private CancellationTokenSource consumersTokenSourcs;
        private CancellationTokenSource producerTokenSourcs;
        private Task producerTask;
        private ActionBlock<int>[] consumers;

        public Program( int consumersCount)
        {
            ConsumersCount = consumersCount;

            buffer = new BufferBlock<int>(new DataflowBlockOptions { BoundedCapacity = 1});

            ConsumerOptions = new ExecutionDataflowBlockOptions { BoundedCapacity = 1, };
            LinkOptions = new DataflowLinkOptions { PropagateCompletion = true, };
            consumers = new ActionBlock<int>[ConsumersCount];
        }


        public int ConsumersCount { get; set; }

        public ExecutionDataflowBlockOptions ConsumerOptions { get; set; }
        public DataflowLinkOptions LinkOptions { get; set; }

        private void StopConsumers()
        {
            foreach (var item in consumers)
            {
                item.Complete();
            }
            consumersTokenSourcs.Cancel();
            consumersTokenSourcs = null;
        }

        private void StopProducer()
        {
            producerTokenSourcs.Cancel();
            producerTokenSourcs = null;
        }

        public void WaitProducer()
        {
            producerTask.Wait();
        }

        public async Task StartProducerAsync()
        {
            if (producerTokenSourcs != null)
            {
                Console.WriteLine("Producer already started");
            }
            producerTokenSourcs = new CancellationTokenSource();
            var token = producerTokenSourcs.Token;
            producerTask = Task.Factory.StartNew(() => {
                var rnd = new Random();

                while (!token.IsCancellationRequested)
                {
                    var value = rnd.Next();
                    buffer.SendAsync(value);
                }

                buffer.Complete();
            }, token);
            await producerTask.ConfigureAwait(true);
        }



        public async Task StartConsumersAsync()
        {
            if (consumersTokenSourcs != null)
            {
                Console.WriteLine("Consumers already started");
            }
            consumersTokenSourcs = new CancellationTokenSource();

            var token = consumersTokenSourcs.Token;
            try
            {
                for (int i = 0; i < ConsumersCount; i++)
                {
                    var index = i;
                    var consumer = new ActionBlock<int>(data =>
                    {
                        token.ThrowIfCancellationRequested();

                        if (!token.IsCancellationRequested)
                        {
                            Console.WriteLine("Consumer {0} received {1}", index, data);
                        }
                    }, new ExecutionDataflowBlockOptions { BoundedCapacity = 1, });

                    buffer.LinkTo(consumer, LinkOptions);
                    consumers[i] = consumer;
                }
                var tasks=consumers.Select(x => x.Completion).ToArray();


                var canceled=Task.WhenAll(tasks).ContinueWith(x =>
                {
                    Console.WriteLine("Consumer task is canceled");
                },TaskContinuationOptions.OnlyOnCanceled);


                var errors = Task.WhenAll(tasks).ContinueWith(x =>
                {
                    HandleExceptions(x.Exception);
                }, TaskContinuationOptions.OnlyOnFaulted);

                await Task.WhenAll(canceled, errors);
            }
            catch (AggregateException ex)
            {
                HandleExceptions(ex);
            }
            catch (TaskCanceledException)
            {

                Console.WriteLine("Tasks has been canceled");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something strange is happened: {0}", ex);
                throw;
            }

        }

        private void HandleExceptions(AggregateException ex)
        {
            var exceptions = ex.InnerExceptions;
            if (Enumerable.All(exceptions, x => x is TaskCanceledException))
            {
                Console.WriteLine("Tasks  has been canceled");
                return;
            }
            foreach (var exception in exceptions)
            {
                Console.WriteLine("An error happened during an task execution: {0}", exception.ToString());
            }
        }
    }


}



